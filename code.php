
<?php 


class Building{


	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this -> name = $name;
		$this -> floors = $floors;
		$this -> address = $address;
	}

	// getter (accessors)
    public function getName()
    {return $this->name;}

    // setter (mutators)
    public function setName($name)
    {$this->name = $name;}

    // getter (accessors)
    public function getFloor()
    {return $this->floors;}

    // setter (mutators)
    public function setFloor($floors)
    {$this->floors = $floors;}

    // getter (accessors)
    public function getAddress()
    {return $this->address;}

    // setter (mutators)
    public function setAddress($address)
    {$this->address = $address;}

}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{
  

}

$condominium = new Condominium('Mojo Condo', 5, 'Mabini, Makati City, Philippines');



