<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04 Activity: Access Modifiers</title>
</head>
<body>
	<h2>Building</h2>

	<p> The name of the building is <?php echo $building->getName();?></p>
	<p> The <?php echo $building->getName();?> has <?php echo $building->getFloor();?> floors</p>

	<p> The <?php echo $building->getName();?> is located at <?php echo $building->getAddress();?></p>


	<?php $building->setName('Caswynn Complex');?>
	<p>The name of the building has been changed to <?php echo $building->getName();?> </p>

		
	<h2>Condominium</h2>

	<p>The name of the condominium is <?php echo $condominium->getName() ;?></p>

	<p> The <?php echo $condominium->getName();?> has <?php echo $condominium->getFloor();?> floors</p>

	<p> The <?php echo $condominium->getName();?> is located at <?php echo $condominium->getAddress();?></p>	

	<?php $condominium->setName('Magic Tower');?>	
	<p>The name of the condominium has been changed to <?php echo $condominium->getName();?> </p>


</body>
</html>
